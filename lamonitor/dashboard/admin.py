from django.contrib import admin

from .models import Server

class ServerAdmin(admin.ModelAdmin):
  """ Server model administration model. leol. """

  pass

admin.site.register(Server,ServerAdmin)

from django.conf.urls import patterns, url
from django.views.generic import TemplateView, DetailView

from .models import Server

urlpatterns = patterns('',
    url(r'server/(?P<pk>\d+)$', DetailView.as_view(model=Server), 
      name="dashboard-server"),


    url(r'^$', TemplateView.as_view(template_name='dashboard/index.html'), 
      name="dashboard-index"),
)

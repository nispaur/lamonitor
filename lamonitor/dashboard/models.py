# coding: utf-8


from annoying.fields import JSONField

from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone

# Create your models here.

class Server(models.Model):
  """ Class representing a (monitored) server  """

  hostname = models.CharField(max_length=255, verbose_name="Server hostname")
  ip = models.GenericIPAddressField(verbose_name="IP Address")

  date_created = models.DateTimeField(verbose_name="Date created", null=True,
   auto_now_add=True, default=timezone.now)

  date_modified = models.DateTimeField(verbose_name="Date modified", null=True,
   auto_now=True, default=timezone.now)

  is_active = models.BooleanField(verbose_name="Active server", default=True)

  def __str__(self):
    return self.hostname

  def get_absolute_url(self):
    return reverse('dashboard-server', args=[self.pk])

class Probe(models.Model):
  """
    A probe, a monitored group of values. Is always linked to a specific server.
    Example: All disk reads and writes for host W.X.Y.Z

  """

  display_name = models.CharField(max_length=255, verbose_name="Probe name")

  last_updated = models.DateTimeField(verbose_name="Last update", null=True,
    auto_now=True, default=timezone.now)


class ProbeReport(models.Model):
  """
    A single report (a fetched asset of values)
    Associated to a Probe and a timestamp
    Stores the data in a JSONField (from django-annoying)

    TODO: Async gestion (double timestamp, at the start AND end of the job)

  """

  date_created = models.DateTimeField(verbose_name="Last update", null=True,
    auto_now=True, default=timezone.now)
  data = JSONField()